# -*- coding: utf-8 -*-

'''Password management with Bitwarden.'''

import json
import subprocess
import os
from shutil import which

from albert import *

__iid__ = 'PythonInterface/v0.4'
__doc__ = 'Password management with Bitwarden.'
__title__ = 'Bitwarden'
__version__ = '0.0.2'
__triggers__ = 'bw '
__authors__ = 'Emilio Martin Lundgaard Lopez'
__exec_deps__ = ['bw']


if which('bw') is None:
    raise Exception('"bw" is not in $PATH.')

extensionPath = os.path.dirname(__file__)
iconPath = '{}/logo.svg'.format(extensionPath)
session_file_path = '{}/session'.format(extensionPath)


def get_session_key():
    if os.path.exists(session_file_path):
        with open(session_file_path, 'r') as file:
            data = file.read()
            data_lines = data.split('\n')
            try:
                session_line = data_lines[7]
                session_key = session_line.split('--session ')[-1]
                return session_key
            except:
                return None
    return None


def handleQuery(query):
    if query.isTriggered:
        items = []
        entries = query.string.split(' ')
        command = entries[0]
        if command == '':
            return []

        session_key = get_session_key()

        if 'unlock'.startswith(command) or session_key is None:
            items.append(Item(
                id='unlock',
                icon=iconPath,
                text='Unlock vault',
                subtext='Fires up a safe terminal for you to gimme your pass, son',
                completion='{}unlock'.format(query.trigger),
                actions=[
                    TermAction(
                        text='Unlock',
                        commandline=['bw', 'unlock',
                                     '>{}/session'.format(extensionPath)]
                    )
                ]
            ))
        elif not session_key is None:
            search_items = subprocess.check_output(
                ['bw', 'list', 'items', '--search', command, '--session', session_key])
            search_items = json.loads(search_items)

            for item in search_items:
                actions = []

                if not item['login']['password'] is None:
                    actions.append(
                        ClipAction('Copy password', item['login']['password']))

                if not item['login']['username'] is None:
                    actions.append(
                        ClipAction('Copy username ({})'.format(item['login']['username']), item['login']['username']))

                items.append(Item(
                    id=item['id'],
                    icon=iconPath,
                    text=item['name'],
                    subtext=item['name'],
                    actions=actions
                ))

            if 'sync'.startswith(command):
                items.append(Item(
                    id='sync',
                    icon=iconPath,
                    text='Sync vault',
                    subtext='Synchronize vault with canonical version.',
                    completion='{}sync'.format(query.trigger),
                    actions=[
                        FuncAction('Sync', lambda: sync(session_key))
                    ]
                ))

            items.append(Item(
                id='lock',
                icon=iconPath,
                text='Lock vault',
                subtext='Lock that mofo, buddy.',
                completion='{}lock'.format(query.trigger),
                actions=[
                    FuncAction('Lock', lock)
                ]
            ))

        return items


def lock():
    subprocess.Popen(['bw', 'lock'])
    if os.path.exists(session_file_path):
        os.remove(session_file_path)


def sync(session_key):
    subprocess.Popen(['bw', 'sync', '--session', session_key])


def initialize():
    lock()


def finalize():
    lock()
